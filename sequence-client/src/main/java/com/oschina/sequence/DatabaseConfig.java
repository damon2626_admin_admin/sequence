package com.oschina.sequence;

/**
 * Created by limu on 15/10/9.
 */
public class DatabaseConfig {
    /**
     * 数据库地址
     */
    private String host;

    /**
     * 数据库用户
     */
    private String userName;

    /**
     * 数据库密码
     */
    private String password;

    /**
     * 数据库名称
     */
    private String databaseName;


    public String getHost() {
        return host;
    }

    public String getJdbcUrl() {
        return "jdbc:mysql://" + host + "/" + databaseName + "?useUnicode=true&characterEncoding=utf8&autoReconnect=true&&zeroDateTimeBehavior=convertToNull";
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }
}
